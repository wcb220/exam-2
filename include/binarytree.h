
//Question 3
typedef struct{
    void* val;
    Node** children;
}Node;

//Question 4
typedef struct{
    Node* node;
    char depth;
}Btree;

//Question 5
void initBinaryTree(Btree* btree_pointer);

bool insertItem(Btree* btree_pointer, void* item);

void* removeItem(Btree* btree_pointer, void* item);

int findItem(Btree* btree_pointer, void* item);

void freeNodes(Btree* btree_pointer);

void printTree(Btree* btree_pointer);
