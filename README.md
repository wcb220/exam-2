# CSE 109 - Midterm Exam II

Spring 2020

## Ethics Contract

**FIRST**: Please read the following carefully:

-	I have not received, I have not given, nor will I give or receive, any assistance to another student taking this exam, including discussing the exam with students in another section of the course. Do not discuss the exam after you are finished until final grades are submitted.
-	I will not plagiarize someone else's work and turn it in as my own. If I use someone else's work in this exam, I will cite that work. Failure to cite work I used is plagiarism.
-	I understand that acts of academic dishonesty may be penalized to the full extent allowed by the Lehigh University Code of Conduct, including receiving a failing grade for the course. I recognize that I am responsible for understanding the provisions of the Lehigh University Code of Conduct as they relate to this academic exercise.

If you agree with the above, type your full name in the following space along with the date. Your exam **will not be graded** without this assent.

*** Writing your name and the date below binds you to the above agreement

William Barish, 4/20/2020

*** Writing your name and the date above binds you to the above agreement

## Prelude

Now... on to the exam. There are two parts to this exam. In part 1 you're going to implement a binary tree data structure in C. If you need a refresher on binary trees [here's a good one](https://en.wikipedia.org/wiki/Binary_search_tree) that has implementations on the general algorithms. There's also literally a million videos on you tube about binary trees.

In the last two homeworks I gave you a skeleton project to get you started. For this exam, you're going to work from scratch. This repository is blank except for the readme. You are going to lay out the directory structure, create a Makefile, create a header file, create a library file, and then implement your tree, and then finally provide tests to verify the tree works. You can use any libraries and functions you like (except one implementing a binary tree of course!).

In part 2 I'll ask you to answer a couple open ended questions.

Some ground rules though: I'm going to divide the project into discrete "questions". E.g. question 1 will be to create blank files to flesh out your directory structure. After each question you need to commit your progress to gitlab. This is how I will know you are not just copying and pasting an entire solution from the internet. I will be able to see how much time you spend on each question based on your commit times. For each question, I want you to copy and paste the hash of the commit (you can copy the hash of the commit from the commit menu after you push to gitlab into this README. See this image: https://imgur.com/fSIRINb. The hash of the commit in the example image looks like this: 0258b14275288e117b704cce59d8cb7bb7b2c3c4). This means for each question you will have two commits: one to commit the work, and another to commit the hash into this readme. Each commit message will be the associate question number. For Question 1 the commit message will be "Question 1". And the subsequent hash will be "Question 1 hash".

If you complete a question and need to go back and modify code in a previous question that's okay. The hash should just represent your first commit of that code.

Each question will be worth the same number of points, for a total of 100 points for all questions. 

Also, **and this is very important** if you use any information from the internet or anywhere else, you have to **CITE IT** in your exam. This includes the wiki article I posted above. If you use that, cite it every time you use it. I don't care about the format, just make it clear.

## Part 1

### Question 1

Lay out your directory. Create a src folder, a include folder, and a blank Makefile. 

- Inside the `src` folder create a `bin` directory. 
- Inside the `src/bin` directory create `test.c`
- Inside the `src` directory create `lib.c`
- Inside the `include` directory, create `binarytree.h`


Hash: 251479cb5db1bfd57135a1782979cb4e0d38f9d7

### Question 2

Implement the Makefile to build your project. It will be a little different than the HW so pay attention to these instructions.

- `make static` will generate a static library (libbinarytree.a) and put it in `build/lib`
- `make shared` will generate a shared library (*libbinarytree.so) and put it in `build/lib`
- `make test` will generate a test executable from test.c and statically link to libbinarytree.a (it should make that as well if it doesn't exist). But that in `build/bin`
- `make clean` will clean the project of all build artifacts (*.so *.a *.o test)

Hash: 34cc478052747f70d165cf3ad3603a003f7a7830

### Question 3

In `binarytree.h`, write a struct to represent the nodes of the binary tree. Each node should have at least:

- `void*` a pointer to an item held on the node
- `Node**` An array of pointers to other nodes

You can include any other fields you see fit to make this work.

Hash: e268cac729b6d4e08ece050e6ca81558d6d830f7

### Question 4

In `binarytree.h`, write a struct to represent the binary tree. The binary tree should have at least:

- `Node*` a pointer to the root node
- `char` a character to hold the depth of the tree.

You can include any other fields you see fit to make this work.

Hash: 83023e5d716accffe47be779978137ae632d0ec2

### Question 5

Stub out the relevant functions for your binary tree in `binarytree.h`

The functions you will implement are:

- `initBinaryTree()` takes a binary tree pointer and initialize the values on it.
- `insertItem()` takes a pointer to a binary tree and a pointer to an item and insert it into the tree. Returns `true` on success and `false` on failure (think about under what condition an insert would fail. It's probably not common). Updates the depth on the tree after each insert.
- `removeItem()` takes a pointer to a binary tree and a pointer to an item and remove that item from the tree if it exists in the tree. Returns a pointer to the item if the item was removed and `NULL` if the item was not removed (because it's not there). Updates the depth of the tree after each remove.
- `findItem()` takes a pointer to a binary tree and a pointer to an item and returns the depth of the item. It returns -1 if the item is not found in the list.
- `freeNodes()`takes a pointer to a binary tree and free all the nodes. It will not free the binary tree pointer.
- `printTree()` takes a pointer to a binary tree and print the entire tree. This can be in any format that helps you visualize the tree.

You can add any other function arguments you see fit to make your functions work.

Hash: f7d31e796ab9ffabceff59f908ff149a22c72240

### Question 6

Implement `initBinaryTree()` in `lib.c`

Hash: ab3dc954bc1e8082c4bb28f588615100e6d81fd8

### Question 7

Implement `insertItem()` in `lib.c`

Hash: 6321e4706cfaa93b3bf81f1642bc4ab62c7b2082

### Question 8

Implement `printTree()` in `lib.c`

Hash: 83125180fb68a5a1cd9acc6dbe457549dc1b2c62

### Question 9

Implement `findItem()` in `lib.c`

Hash: 8c806bc2083159018a24cc5cdb2666a5cc0c0cde

### Question 10

Implement `removeItem()` in `lib.c`

Hash: 73f0279c66519806cb103935de56cb7af276d95f

### Question 11

Implement `freeNodes()` in `lib.c`

Hash: f090e871c224d5274fe12668e0c1574caf3275ed

### Question 12

Write at least 5 tests in `test.c` to verify your code works.

Hash: 49994601141a1837cd283a1144fb61d760741d2d

### Bonus

Set up a test runner on gitlab so that your test runs automatically and passes with a green checkmark.

Hash:

## Part 2

Now answer the following questions. You can commit the answers here in this file. You'll do two commits again, one for the content, and one for the hash of the commit. These questions are very open ended. I want you to write as much as you can to convince me you know what you're talking about. Minimum 100 words for each question.

### Question 13

What is the difference between the stack and the heap? What variables go on the stack? What variables go on the heap?

	A stack and a heap differ when it comes to memory allocation. A stack will have a set block of memory available, and the blocks of memory are all next to
	each other. This is a set size determined before a program runs. The computer will automatically manage the variables on a stack, so there is no need for
	a programmer to free any variables on a stack. The types of variables that go on a stack are ones that are created by a function, and once the function is
	completed the computer will automatically free those variables. A heap will allocate memory in any order, and does not have to be next to one another like
	a stack. Heap memory allocation is determined by the person who writes the program. This means that the type of variables that go on a heap have to use the
	malloc() function so you can allocate memory for it. Variables on the heap are not limited to the function from which they were created.

Hash: 5a078c274381b43e154932ade3d8761c1a8b7e12

### Question 14

What are the pros of hash tables? What are the cons? What are some applications where hash tables are useful, and why are they good for those applications?

	Hash tables are very good because they are extremely efficienty when looking for a value. For example, we talked about a website that has thousands to
	millions of users, if you looked up the login and password information linearly, it would have a time complexity of O(n), whereas the hash table has O(1) 
	time complexity. The cons of hash tables however are collisions. It is nearly impossible to avoid a collision with a hash table, which is when a value
	is mapped to a memory location that already has a value. As I said earlier, hash tables are great for large sets of memory like when you have to save user
	information when there are a lot of usersm since it decreases the lookup time dramatically.

Hash: 5deb96d125ba94201fa670ca382545cc1b0d8b83

### Question 15

Create a segfault in your binary tree program and commit it to gitlab. What are the steps you would use in gdb to debug this segfault? How do you compile your program to use with gdb? Give the output of a stack trace at the point at which the segfault occurs. Fix the segfault again and commit the fix.

Hash of segfault: 331be768490461e018b285f1a45ccafdde069e7f
Hash of fix: ebd3c2d8be5e72ca362f62d53680d038f08049eb
Hash of Question 15 answer: I would use gdb to debug the program by setting a break point before the line where the segfault occurs. When this is done, you can
	step through the program line by line and inspect the values of everything. Once you get to the segfault, you can inspect the variables and see why you
	had the segfault. In order to use this feature, you should add a "-g" (it would be gcc -g ...) in the gcc commands in your makefile for when you make the 
	.a, .o, and executable files. Then you write gdb test, for example, which will start the debug process for the file and then write run which will start it. 
	The backtrace of where the segfault occurs is the following:
		#0  __GI___libc_malloc (bytes=8) at malloc.c:3028
		#1  0x0000000008000b82 in insertAtRoot (btree_pointer=0x8404270, item=0x7ffffffee364) at src/lib.c:15
		#2  0x00000000080008b6 in main () at src/bin/test.c:33