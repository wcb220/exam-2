#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "binarytree.h"


void initBinaryTree(Btree* btree_pointer){
    btree_pointer->root = NULL;
    btree_pointer->depth = 0;
}

bool insertAtRoot(Btree* btree_pointer, void* item){

    struct Node* newNode = malloc(sizeof(struct Node*));
    newNode->val = item;
    newNode->left = NULL;
    newNode->right = NULL;

    if (btree_pointer->root == NULL){
        btree_pointer->root = newNode;
        btree_pointer->depth++;
        return true;
    }
    else{
        bool insertFlag = insertItem(btree_pointer->root, item, btree_pointer);
        return insertFlag;
    }

}

bool insertItem(struct Node* root, void* item, Btree* btree_pointer){

    struct Node* newNode = malloc(sizeof(struct Node*));
    
    newNode->val = item;
    newNode->left = NULL;
    newNode->right = NULL;

    if(btree_pointer->root->val == item){
        return false;
    }

    if (item < root->val && root->left == NULL){
        root->left = newNode;
        btree_pointer->depth++;
        return true;
    }
    else if (item > root->val && root->right == NULL){
        root->right = newNode;
        btree_pointer->depth++;
        return true;
    }
    else if (item < root->val && root->left != NULL){
        return insertItem(root->left, item, btree_pointer);
    }
    else if (item > root->val && root->right != NULL){
        return insertItem(root->right, item, btree_pointer);
    }
    return false;

}



void printTree(struct Node* root){
    
    if (root != NULL){
        //void* temp = root->val;
        printf("%d\n", *(int *)root->val);
        if(root->left != NULL){
            return printTree(root->left);
        }
        if(root->right != NULL){
            return printTree(root->right);
        }
    }

}


bool findItem(struct Node* root, void* item){
    if (root->val == item){
        return true;
    }
    else if (root->left != NULL){
        return findItem(root->left, item);
    }
    else if (root->right != NULL){
        return findItem(root->right, item);
    }
    return false;
}


void* removeItem(struct Node* root, void* item){

    if (root->val == item){
        struct Node* next = root->right;
        struct Node* prev = malloc(sizeof(struct Node*));
        while(next->left != NULL){
            prev = next;
            next = next->left;
        }
        prev->left = NULL;
        next->left = root->left;
        next->right = root->right;
        return root;
    }
    else if (item < root->val){
        return removeItem(root->left, item);
    }
    else if (item > root->val){
        return removeItem(root->right, item);
    }
    return NULL;
}

void freeNodes(struct Node* root){

    if (root->left != NULL){
        freeNodes(root->left);
        free(root->left);
    }
    if (root->right != NULL){
        freeNodes(root->right);
        free(root->right);
    }

}

