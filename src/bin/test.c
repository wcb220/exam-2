#include "binarytree.h"
#include "stdbool.h"
#include <stdlib.h>
#include <stdio.h>

int test(char* label, bool a, bool b) {
  printf("%s: ", label);
  if (a==b) {
    printf("Passed\n");
    return 1;
  } else {
    printf("Failed. lhs=%x, rhs= %x\n",a,b);
    exit(1);
  }
}

int main(){
    
    printf("%s\n", "hello");

    Btree *binTree; 
    binTree = (Btree*) malloc(sizeof(Btree));

    int a = 5;
    int b = 3;
    int c = 7;
    int d = 1;
    int e = 2;
    int f = 6;
    int g = 8;
    bool temp = true;
    
    test("Test 1", 1, insertAtRoot(binTree, &a));
    test("Test 2", 1, insertItem(binTree->root, &b, binTree));
    test("Test 3", 1, insertItem(binTree->root, &c, binTree));
    test("Test 4", 1, insertItem(binTree->root, &d, binTree));
    test("Test 5", 1, insertItem(binTree->root, &e, binTree));
    test("Test 6", 1, insertItem(binTree->root, &f, binTree));
    test("Test 7", 1, insertItem(binTree->root, &g, binTree));
    printTree(binTree->root);
    test("Test 8", 1, findItem(binTree->root, &a));
    test("Test 9", 1, findItem(binTree->root, &b));
    test("Test 10", 1, findItem(binTree->root, &c));
    test("Test 11", 1, findItem(binTree->root, &d));
    test("Test 12", 1, findItem(binTree->root, &e));
    test("Test 13", 1, findItem(binTree->root, &f));
    test("Test 14", 1, findItem(binTree->root, &g));

    free(a);
    a = a+1;


}